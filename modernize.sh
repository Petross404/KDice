#!/bin/bash

clang-tidy -header-filter='.*' -checks='-*,modernize-avoid-bind' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-deprecated-headers' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-loop-convert' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-make-shared' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-make-unique' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-pass-by-value' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-raw-string-literal' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-redundant-void-arg' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-replace-auto-ptr' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-shrink-to-fit' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-auto' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-bool-literals' -fix
# clang-tidy -header-filter='.*' -checks='-*,modernize-use-default-member-init' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-emplace' -fix
# clang-tidy -header-filter='.*' -checks='-*,modernize-use-equals-default' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-equals-delete' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-nullptr' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-override' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-transparent-functors' -fix
clang-tidy -header-filter='.*' -checks='-*,modernize-use-using' -fix
