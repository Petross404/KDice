#!/bin/sh

astyle --indent=spaces=4 --style=linux \
       --indent-labels --pad-oper --unpad-paren --pad-header \
       --keep-one-line-statements --convert-tabs \
       --indent-preprocessor \
       `find -type f -name '*.cpp' -or -name '*.cc' -or -name '*.h'  -or -name '*.h**'`

rm -v src/*.*.orig
rm -v src/*/*.*.orig

