# Install script for directory: C:/Users/Petros/projects/KDice.git

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/KDice")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xKDicex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/data/icons/hicolor/128x128/apps" TYPE FILE RENAME "dice.png" FILES "C:/Users/Petros/projects/KDice.git/src/resources/images/128-apps-dice.png")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xKDicex" OR NOT CMAKE_INSTALL_COMPONENT)
  
    set(DESTDIR_VALUE "$ENV{DESTDIR}")
    if (NOT DESTDIR_VALUE)
        execute_process(COMMAND "C:/Program Files/CMake/bin/cmake.exe" -E touch "C:/Program Files (x86)/KDice/bin/data/icons/hicolor")
        set(HAVE_GTK_UPDATE_ICON_CACHE_EXEC GTK_UPDATE_ICON_CACHE_EXECUTABLE-NOTFOUND)
        if (HAVE_GTK_UPDATE_ICON_CACHE_EXEC)
            execute_process(COMMAND GTK_UPDATE_ICON_CACHE_EXECUTABLE-NOTFOUND -q -t -i . WORKING_DIRECTORY "C:/Program Files (x86)/KDice/bin/data/icons/hicolor")
        endif ()
    endif (NOT DESTDIR_VALUE)
    
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xKDicex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "C:/Users/Petros/projects/KDice.git/build/bin/KDice.exe")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xKDicex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/data/applications" TYPE PROGRAM FILES "C:/Users/Petros/projects/KDice.git/KDice.desktop")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "C:/Users/Petros/projects/KDice.git/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
