/****************************************************************************
** Meta object code from reading C++ file 'kdice.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../src/kdice.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kdice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KDice_t {
    QByteArrayData data[16];
    char stringdata0[182];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KDice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KDice_t qt_meta_stringdata_KDice = {
    {
QT_MOC_LITERAL(0, 0, 5), // "KDice"
QT_MOC_LITERAL(1, 6, 11), // "readyToRoll"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 26), // "isNumberOfRollsIncremented"
QT_MOC_LITERAL(4, 46, 6), // "answer"
QT_MOC_LITERAL(5, 53, 18), // "qmovieFrameChanged"
QT_MOC_LITERAL(6, 72, 7), // "QMovie*"
QT_MOC_LITERAL(7, 80, 5), // "movie"
QT_MOC_LITERAL(8, 86, 14), // "disableWidgets"
QT_MOC_LITERAL(9, 101, 13), // "enableWidgets"
QT_MOC_LITERAL(10, 115, 18), // "printNumberOfRolls"
QT_MOC_LITERAL(11, 134, 12), // "printWarning"
QT_MOC_LITERAL(12, 147, 6), // "reload"
QT_MOC_LITERAL(13, 154, 3), // "num"
QT_MOC_LITERAL(14, 158, 10), // "resetKDice"
QT_MOC_LITERAL(15, 169, 12) // "showSettings"

    },
    "KDice\0readyToRoll\0\0isNumberOfRollsIncremented\0"
    "answer\0qmovieFrameChanged\0QMovie*\0"
    "movie\0disableWidgets\0enableWidgets\0"
    "printNumberOfRolls\0printWarning\0reload\0"
    "num\0resetKDice\0showSettings"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KDice[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,
       3,    1,   70,    2, 0x06 /* Public */,
       5,    1,   73,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   76,    2, 0x08 /* Private */,
       9,    0,   77,    2, 0x08 /* Private */,
      10,    0,   78,    2, 0x08 /* Private */,
      11,    0,   79,    2, 0x08 /* Private */,
      12,    0,   80,    2, 0x08 /* Private */,
      12,    1,   81,    2, 0x08 /* Private */,
      14,    0,   84,    2, 0x08 /* Private */,
      15,    0,   85,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Bool, QMetaType::Bool,    4,
    QMetaType::Void, 0x80000000 | 6,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void KDice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KDice *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->readyToRoll(); break;
        case 1: { bool _r = _t->isNumberOfRollsIncremented((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 2: _t->qmovieFrameChanged((*reinterpret_cast< QMovie*(*)>(_a[1]))); break;
        case 3: _t->disableWidgets(); break;
        case 4: _t->enableWidgets(); break;
        case 5: _t->printNumberOfRolls(); break;
        case 6: _t->printWarning(); break;
        case 7: _t->reload(); break;
        case 8: _t->reload((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->resetKDice(); break;
        case 10: _t->showSettings(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMovie* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (KDice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KDice::readyToRoll)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = bool (KDice::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KDice::isNumberOfRollsIncremented)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (KDice::*)(QMovie * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KDice::qmovieFrameChanged)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KDice::staticMetaObject = { {
    &KXmlGuiWindow::staticMetaObject,
    qt_meta_stringdata_KDice.data,
    qt_meta_data_KDice,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KDice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KDice::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KDice.stringdata0))
        return static_cast<void*>(this);
    return KXmlGuiWindow::qt_metacast(_clname);
}

int KDice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KXmlGuiWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void KDice::readyToRoll()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
bool KDice::isNumberOfRollsIncremented(bool _t1)
{
    bool _t0{};
    void *_a[] = { const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t0))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
    return _t0;
}

// SIGNAL 2
void KDice::qmovieFrameChanged(QMovie * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
