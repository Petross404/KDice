/********************************************************************************
** Form generated from reading UI file 'resetbtn.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESETBTN_H
#define UI_RESETBTN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ResetBtnWidget
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QCheckBox *checkBox;

    void setupUi(QWidget *ResetBtnWidget)
    {
        if (ResetBtnWidget->objectName().isEmpty())
            ResetBtnWidget->setObjectName(QString::fromUtf8("ResetBtnWidget"));
        ResetBtnWidget->resize(404, 235);
        gridLayout_2 = new QGridLayout(ResetBtnWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox = new QCheckBox(ResetBtnWidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        gridLayout->addWidget(checkBox, 0, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(ResetBtnWidget);

        QMetaObject::connectSlotsByName(ResetBtnWidget);
    } // setupUi

    void retranslateUi(QWidget *ResetBtnWidget)
    {
        ResetBtnWidget->setWindowTitle(QCoreApplication::translate("ResetBtnWidget", "Form", nullptr));
        checkBox->setText(QCoreApplication::translate("ResetBtnWidget", "Enable Reset Button", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ResetBtnWidget: public Ui_ResetBtnWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESETBTN_H
