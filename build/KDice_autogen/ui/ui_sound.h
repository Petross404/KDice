/********************************************************************************
** Form generated from reading UI file 'sound.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOUND_H
#define UI_SOUND_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_soundWidget
{
public:
    QGridLayout *gridLayout_Center;
    QGridLayout *gridLayout;
    QCheckBox *kcfg_chkBoxSound;

    void setupUi(QWidget *soundWidget)
    {
        if (soundWidget->objectName().isEmpty())
            soundWidget->setObjectName(QString::fromUtf8("soundWidget"));
        soundWidget->resize(400, 300);
        gridLayout_Center = new QGridLayout(soundWidget);
        gridLayout_Center->setObjectName(QString::fromUtf8("gridLayout_Center"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        kcfg_chkBoxSound = new QCheckBox(soundWidget);
        kcfg_chkBoxSound->setObjectName(QString::fromUtf8("kcfg_chkBoxSound"));

        gridLayout->addWidget(kcfg_chkBoxSound, 0, 0, 1, 1);


        gridLayout_Center->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(soundWidget);

        QMetaObject::connectSlotsByName(soundWidget);
    } // setupUi

    void retranslateUi(QWidget *soundWidget)
    {
        soundWidget->setProperty("SoundWidget", QVariant(QCoreApplication::translate("soundWidget", "SoundWidget", nullptr)));
        kcfg_chkBoxSound->setText(QCoreApplication::translate("soundWidget", "Enable sound?", nullptr));
    } // retranslateUi

};

namespace Ui {
    class soundWidget: public Ui_soundWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOUND_H
