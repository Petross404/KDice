export PATH=C:/Program Files (x86)/KDice/bin:$PATH

# LD_LIBRARY_PATH only needed if you are building without rpath
# export LD_LIBRARY_PATH=C:/Program Files (x86)/KDice/lib:$LD_LIBRARY_PATH

export XDG_DATA_DIRS=C:/Program Files (x86)/KDice/bin/data:${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}
export XDG_CONFIG_DIRS=C:/Program Files (x86)/KDice/etc/xdg:${XDG_CONFIG_DIRS:-/etc/xdg}

export QT_PLUGIN_PATH=C:/Program Files (x86)/KDice/lib/plugins:$QT_PLUGIN_PATH
export QML2_IMPORT_PATH=C:/Program Files (x86)/KDice/lib/qml:$QML2_IMPORT_PATH
