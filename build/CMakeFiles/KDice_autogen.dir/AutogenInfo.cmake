# Meta
set(AM_MULTI_CONFIG "FALSE")
set(AM_PARALLEL "2")
# Directories
set(AM_CMAKE_SOURCE_DIR "C:/Users/Petros/projects/KDice.git")
set(AM_CMAKE_BINARY_DIR "C:/Users/Petros/projects/KDice.git/build")
set(AM_CMAKE_CURRENT_SOURCE_DIR "C:/Users/Petros/projects/KDice.git")
set(AM_CMAKE_CURRENT_BINARY_DIR "C:/Users/Petros/projects/KDice.git/build")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "ON")
set(AM_BUILD_DIR "C:/Users/Petros/projects/KDice.git/build/KDice_autogen")
set(AM_INCLUDE_DIR "include")
# Files
set(AM_SOURCES "C:/Users/Petros/projects/KDice.git/src/Animation/animation.cpp;C:/Users/Petros/projects/KDice.git/src/Dice/dice.cpp;C:/Users/Petros/projects/KDice.git/src/KDiceHelpers/actions.cpp;C:/Users/Petros/projects/KDice.git/src/KDiceHelpers/connections.cpp;C:/Users/Petros/projects/KDice.git/src/KDiceHelpers/layouts.cpp;C:/Users/Petros/projects/KDice.git/src/KDiceHelpers/read_settings.cpp;C:/Users/Petros/projects/KDice.git/src/KDiceHelpers/widget_helpers.cpp;C:/Users/Petros/projects/KDice.git/src/config/SettingsWidgets/resetbtn.cpp;C:/Users/Petros/projects/KDice.git/src/config/SettingsWidgets/soundWidget.cpp;C:/Users/Petros/projects/KDice.git/src/kdice.cpp;C:/Users/Petros/projects/KDice.git/src/main.cpp")
set(AM_HEADERS "")
set(AM_SETTINGS_FILE "C:/Users/Petros/projects/KDice.git/build/CMakeFiles/KDice_autogen.dir/AutogenOldSettings.txt")
# Qt
set(AM_QT_VERSION_MAJOR "5")
set(AM_QT_MOC_EXECUTABLE "C:/CraftRoot/bin/moc.exe")
set(AM_QT_UIC_EXECUTABLE "C:/CraftRoot/bin/uic.exe")
# MOC settings
set(AM_MOC_SKIP "C:/Users/Petros/projects/KDice.git/build/KDice_autogen/mocs_compilation.cpp;C:/Users/Petros/projects/KDice.git/build/kdicesettings.cpp;C:/Users/Petros/projects/KDice.git/build/kdicesettings.h;C:/Users/Petros/projects/KDice.git/build/qrc_dice.cpp")
set(AM_MOC_DEFINITIONS "ENABLE_RESET;ENABLE_SOUND;GIT_BRANCH=e3239cfbe39a1a92759199f258406b6bac56c1c5;GIT_COMMIT_HASH=e3239cf;KCOREADDONS_LIB;QT_CORE_LIB;QT_DBUS_LIB;QT_GUI_LIB;QT_MULTIMEDIA_LIB;QT_NETWORK_LIB;QT_WIDGETS_LIB;QT_XML_LIB;UNICODE;WIN32_LEAN_AND_MEAN;WINVER=0x0600;_CRT_NONSTDC_NO_DEPRECATE;_CRT_SECURE_NO_DEPRECATE;_CRT_SECURE_NO_WARNINGS;_SCL_SECURE_NO_WARNINGS;_UNICODE;_USE_MATH_DEFINES;_WIN32_IE=0x0600;_WIN32_WINNT=0x0600")
set(AM_MOC_INCLUDES "C:/Users/Petros/projects/KDice.git/build;C:/Users/Petros/projects/KDice.git;C:/Users/Petros/projects/KDice.git/build/KDice_autogen/include;C:/Users/Petros/projects/KDice.git/src;C:/CraftRoot/include/qt5;C:/CraftRoot/include/qt5/QtWidgets;C:/CraftRoot/include/qt5/QtGui;C:/CraftRoot/include/qt5/QtANGLE;C:/CraftRoot/include/qt5/QtCore;C:/CraftRoot/./mkspecs/win32-msvc;C:/CraftRoot/include/qt5/QtMultimedia;C:/CraftRoot/include/qt5/QtNetwork;C:/CraftRoot/include/KF5/KXmlGui;C:/CraftRoot/include/KF5;C:/CraftRoot/include/qt5/QtXml;C:/CraftRoot/include/KF5/KConfigCore;C:/CraftRoot/include/KF5/KConfigWidgets;C:/CraftRoot/include/KF5/KCodecs;C:/CraftRoot/include/KF5/KWidgetsAddons;C:/CraftRoot/include/KF5/KConfigGui;C:/CraftRoot/include/KF5/KAuth;C:/CraftRoot/include/KF5/KCoreAddons;C:/CraftRoot/include/qt5/QtDBus;C:/CraftRoot/include/KF5/KI18n")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE;K_PLUGIN_FACTORY;K_PLUGIN_FACTORY_WITH_JSON;K_PLUGIN_CLASS_WITH_JSON")
set(AM_MOC_DEPEND_FILTERS "K_PLUGIN_FACTORY_WITH_JSON;[
^][ 	]*K_PLUGIN_FACTORY_WITH_JSON[ 	
]*\\([^,]*,[ 	
]*\"([^\"]+)\";K_PLUGIN_CLASS_WITH_JSON;[
^][ 	]*K_PLUGIN_CLASS_WITH_JSON[ 	
]*\\([^,]*,[ 	
]*\"([^\"]+)\"")
set(AM_MOC_PREDEFS_CMD "")
# UIC settings
set(AM_UIC_SKIP "C:/Users/Petros/projects/KDice.git/build/KDice_autogen/mocs_compilation.cpp;C:/Users/Petros/projects/KDice.git/build/qrc_dice.cpp")
set(AM_UIC_TARGET_OPTIONS "")
set(AM_UIC_OPTIONS_FILES "")
set(AM_UIC_OPTIONS_OPTIONS "")
set(AM_UIC_SEARCH_PATHS "")
