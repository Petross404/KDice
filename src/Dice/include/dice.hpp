/*!<
 * \author Petros S <petross404@gmail.com>
 * \file dice.hpp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <random>
#include <iostream>
#include <cstdlib>
#include <functional>
#include <chrono>

/*!
 * \class Dice
 * Dice is a (singleton) class that represents a dice and it's properties.
 */
#ifndef dice_hpp
#define dice_hpp

#include <random>
#include <iostream>
#include <memory>

class Dice		//makes use of Singleton and PIMPL idiom
{
public:
	static Dice* 	instance();	/*!< \brief Create */

	void	roll();			/*!< \brief Roll the dice */
	int	getNumber();		/*!< \brief Return the  m_number */
	void	setNumber( int num );	/*!< \brief Set the dice number
					* \param num is passed to m_number */

	Dice( int num );
	Dice();
	~Dice();

private:
	int 		m_number;
	static Dice*	m_pInstance;
	struct		DiceImplm;
	std::unique_ptr<DiceImplm>	d_dice;
};

#endif /* dice_hpp */
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
