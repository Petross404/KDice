/*!<
* \author Petros S <petross404@gmail.com>
* \file dice.cpp
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*/

#include "include/dice.hpp"

struct Dice::DiceImplm
{
	int m_num;
	std::unique_ptr<std::random_device> rd;
	std::unique_ptr<std::mt19937> e2;
	std::unique_ptr<std::uniform_int_distribution<int>> dis;
	DiceImplm( int num ) : m_num( num ) {}
	DiceImplm()
		: rd( new std::random_device )
		, e2( new std::mt19937( rd.get()->operator()() ) )
		, dis( new std::uniform_int_distribution<int>( 1, 6 ) )
	{
		m_num = dis.get()->operator()( *e2.get() );
	}
};

Dice* Dice::m_pInstance = nullptr;

Dice* Dice::instance()
{
	if ( !m_pInstance ) m_pInstance = new Dice;
	return m_pInstance;
}

Dice::Dice( int num ) : d_dice( new DiceImplm( num ) ) {}

Dice::Dice() { d_dice.reset( new DiceImplm ); }

Dice::~Dice()
{
	delete this->instance();
}

int Dice::getNumber()
{
	if ( ( d_dice->m_num < 1 ) || ( d_dice->m_num > 6 ) )
	{
		std::cerr << "Abort" << std::endl;
		exit( -1 );
	}
	return d_dice->m_num;
}

void Dice::setNumber( int num ) { d_dice->m_num = num; }

void Dice::roll()
{
	d_dice->e2.reset( new std::mt19937( d_dice->rd.get()->operator()() ) );
	d_dice->m_num = d_dice->dis.get()->operator()( *d_dice->e2.get() );
}

// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
