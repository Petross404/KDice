#ifndef VERSION_H
#define VERSION_H
/*!
 * \author Petros S <petross404@gmail.com>
 * \file version.h
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define KDice_VERSION_MAJOR 0
#define KDice_VERSION_MINOR 1
#define KDice_VERSION 0.1

#define Dice_VERSION_MAJOR 1
#define Dice_VERSION_MINOR 0

#define GIT_BRANCH "e3239cfbe39a1a92759199f258406b6bac56c1c5"
#define GIT_COMMIT_HASH "e3239cf"

#endif
