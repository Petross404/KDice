/*!< \author Πέτρος Σιλιγκούνας <petross404@gmail.com> 
 * \file animation.h
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ANIMATION_H
#define ANIMATION_H

#include <QtCore/QEvent>
#include <QtCore/QLoggingCategory> 
#include <QtCore/QScopedPointer>
#include <QtGui/QMovie>
#include <QtGui/QPixmap>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>

Q_DECLARE_LOGGING_CATEGORY(LOG_ANIMATION)

/**
 * @todo write docs
 */
class Animation : public QWidget
{
	Q_OBJECT

public:
	/**
	 * @todo write docs
	 */
	explicit Animation(QWidget* parent);

	/**
	 * @todo write docs
	 */
	~Animation();

	void replay();
	void setupConnectionsOfAnimation();
	void setPixmap(QPixmap p);
	void update(int image_number);			/*!< \brief Update the image of the dice.
							* \param image_num is the random (or not if spinbox is clicked) dice number.*/
private slots:
	void stopAtLastQMovieFrame( QMovie* movie);

signals:
	void qmovieFrameChanged( QMovie* movie );	/*!< \brief Check every frame and stop at the last one.
							* \param movie is pointer to the `QMovie` in question. */
	void startedAnimation();
	void finishedAnimation();

protected:
	/**
	 * @todo write docs
	 *
	 * @param ev TODO
	 * @return TODO
	 */
	void contextMenuEvent( QContextMenuEvent *ev ) override;

private:
	QScopedPointer<QLabel>		m_label;
	QScopedPointer<QGridLayout>	m_gridLayout;
	QScopedPointer<QMovie>		m_movie;
	QScopedPointer<QPixmap>		m_pixmap;
};

#endif // ANIMATION_H
