/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "animation.h"

#include <KI18n/KLocalizedString>
#include <KWidgetsAddons/KMessageBox>

Q_LOGGING_CATEGORY( LOG_ANIMATION, "Animation" )

Animation::Animation( QWidget* parent )
	: QWidget( parent )
	, m_label( new QLabel( parent ) )
	, m_gridLayout( new QGridLayout )
	, m_movie( new QMovie( ":resources/images/bluedice.gif" ) )
	, m_pixmap( new QPixmap( ":/resources/images/dice.png" ) )
{
	m_gridLayout->addWidget( m_label.data(), 0, 0 );
	setLayout( m_gridLayout.data() );
	setupConnectionsOfAnimation();

	m_label->setPixmap( *m_pixmap.data() );
	m_label->setBackgroundRole( QPalette::Base );
	m_label->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
	m_label->setScaledContents( true );
	m_label->setFrameStyle( QFrame::StyledPanel | QFrame::Raised );
	m_label->setLineWidth( 2 );

	show();
}

Animation::~Animation() = default;

void Animation::replay()
{
	m_label->setMovie( m_movie.data() );
	Q_ASSERT_X( m_movie->isValid(), "Animation::Animation(QWidget* parent)", "Invalid animation" );

	if ( Q_UNLIKELY( !m_movie->isValid() ) )
	{
		QString animationError {"Animation type is not supported! Did you forget to include *.qrc file?"};
		m_label->setText( tr( animationError.toLocal8Bit().constData() ) );
		QMessageBox::critical( this, tr( "Error" ), tr( animationError.toLocal8Bit().constData() ) );
		exit( -1 );
	}

	m_movie->start();

	if ( ( m_movie->state() == QMovie::Running ) &&
	                ( m_movie->currentFrameNumber() == ( 0 ) ) )
		emit startedAnimation();
}

void Animation::setupConnectionsOfAnimation()
{
	//Thanks to the guys at this thread :
	//https://forum.qt.io/topic/88197/custom-signal-to-slot-the-slot-requires-more-arguments-than-the-signal-provides
	connect( m_movie.data(), &QMovie::frameChanged,
	         this, std::bind( &Animation::qmovieFrameChanged,
	                          this,
	                          m_movie.data()
	                        )
	       );

	connect( this, &Animation::qmovieFrameChanged, this, &Animation::stopAtLastQMovieFrame );
	connect( m_movie.data(), &QMovie::finished, this, &Animation::finishedAnimation );
}

// Check if the frame is the last one and stop the dice animation
void Animation::stopAtLastQMovieFrame( QMovie* movie )
{
	if ( movie->currentFrameNumber() == ( movie->frameCount() - 1 ) )
	{
		movie->stop();

		//Explicity emit finished signal
		//https://bugreports.qt.io/browse/QTBUG-66733
		if ( movie->state() == QMovie::NotRunning )
		{
			emit movie->finished();
			emit finishedAnimation();
		}
	}
}

/// This function accepts an image_number which is already have been
/// generated randomly in Dice class members. Based on image_number it
/// selects the proper dice image to show
void Animation::update( int imageNumber )
{
	//Now deal with which image will be loaded based on image_number
	//The whole point of this program is here

	if ( Q_UNLIKELY( ( imageNumber < 0 ) || ( imageNumber > 6 ) ) )
	{
		qCCritical( LOG_ANIMATION ) << "The number" << imageNumber << "is out of range.";
		QString numberMsgError = "A dice doesn't have number " + ( QString( "%1" ).arg( imageNumber ) )
		                         + ". This shouldn't have happened.";

		QString detailedMsgError = "KDice is using Dice engine, which is based on C++'s"
		                           "std::random_device. Dice::getNumber should catch any such error.";

		KMessageBox::detailedError( this,
		                            i18n( numberMsgError.toLatin1() ),
		                            i18n( detailedMsgError.toLatin1() ),
		                            i18n( "Wrong number" ),
		                            KMessageBox::Option::Notify );
		exit( -1 );
	}

	m_pixmap->load( QString( ":/resources/images/dice-%1.png" ).arg( imageNumber ) );
	Q_ASSERT_X( !m_pixmap->isNull(), "Pixmap is null", "void Animation::update(int)" );
	m_label->setPixmap( *m_pixmap.data() );
}

void Animation::setPixmap(QPixmap p)
{
	m_label->setPixmap(p);
}

void Animation::contextMenuEvent( QContextMenuEvent* event )
{
	QMenu menu;
}

// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
