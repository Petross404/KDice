#ifndef KDICE_H
#define KDICE_H

/*!< \author Petros S <petross404@gmail.com> 
 * \file kdice.h
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "Dice/include/dice.hpp"
#include "Animation/animation.h"
#include "version.h"
#include <iostream>

#include <QtCore/QLoggingCategory>
#include <QtCore/QStandardPaths>
#include <QtGui/QMovie>
#include <QtWidgets/QAction>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QShortcut>

// Why include this if -DENABLE_SOUND=OFF?
#ifdef ENABLE_SOUND
	#include <QtMultimedia/QSoundEffect>
#endif

#include <KXmlGui/KActionCollection>
#include <KConfigCore/KConfig>
#include <KConfigCore/KSharedConfig>
#include <KConfigWidgets/KConfigDialog>
#include <KConfigGui/KConfigSkeleton>
#include <KI18n/KLocalizedString>
#include <KWidgetsAddons/KPageWidget>
#include <KConfigWidgets/KStandardAction>
#include <KXmlGui/KXmlGuiWindow>

#include <kdicesettings.h>
#include <KConfigDialogManager>
#include "config/SettingsWidgets/soundWidget.h"
#include "config/SettingsWidgets/resetbtn.h"

Q_DECLARE_LOGGING_CATEGORY(LOG_KDICE)

using KD_InterSettings = KDiceSettings::InternalSettings;		/*!< `KDice::InternalSettings` is tedious */
using InterSettingsPtr = QSharedPointer<KD_InterSettings>;	/*!< Pointer to `InternalSettings` */

/*!
 * \class KDice
 * KDice is a big monolithic class that inherits from `QWidget` and implements a dice rolling application.
 */
class KDice : public KXmlGuiWindow
{
    Q_OBJECT

public:
	explicit KDice(QWidget *parent = nullptr);	/*!<
							* \brief Is accepting the following :
							* \param number which is an `int`
							* \param parent the parent widget of `KDice`  */

	~KDice();			/*!< \brief This is empty as memory is handled by Qt with `QScopedPointer`.*/
	void animateDice();		/*!< \brief Play the rolling dice.*/

	void loadSettings();		/*!< \brief Load settings */
	void saveSettings();		/*!< \brief Save settings */

private slots:
	void disableWidgets();		/*!< \brief Disable widgets during rolling animation. */
	void enableWidgets();		/*!< \brief Re-enable widgets after the animation is stopped. */
	void printNumberOfRolls();	/*!< \brief Print how many times the dice is rolled. */
	void printWarning();		/*!< \brief Print a message that user cheated with spinbox */
	void reload();			/*!< \brief The very core of KDice. It calls Dice::roll	 */
	void reload(int num);		/*!< \overload void reload(int num)
					 * \brief This is an overloaded function. 
					 * It accepts a predefined int and doesn't produce a random value!*/
	void resetKDice();		/*!< \brief This resets the m_label and various widgets. */
	void showSettings();		/*!< \brief Opens up a KDE configuration window. */

signals:
	void readyToRoll();				/*!< \brief signal the app that a roll is about to happen. */
	bool isNumberOfRollsIncremented(bool answer);	/*!< \brief signal the app that another rolling took place.
							 * \param answer is a variable passed to `QAction::setDisabled`.
							 * \return A Boolean that answers if number of rolls is incremented */

	void qmovieFrameChanged(QMovie *movie);		/*!< \brief signal the app that another `QMovie` frame is changed.
							 * \param movie is a pointer to the `QMovie` in question. */

private:
	int 				m_numberOfRolls;	/*!< How many times the dice is rolled? */
	int				m_image_number;		/*!< Number of image to show after reload */
	QScopedPointer<QPixmap>		m_image;		/*!< The image that is shown in KDice */
	QScopedPointer<QPushButton>	m_btnRoll;		/*!< `QPushButton` Roll */
	QScopedPointer<QPushButton>	m_btnQuit;		/*!< `QPushButton` Quit */
	QScopedPointer<QWidget>		m_widgetCentral;	/*!< Qt's central widget */
	QScopedPointer<QGridLayout>	m_gridLayout;		/*!< Central layout which every other is sitting on */
	QScopedPointer<QGridLayout>	m_gridLabel;		/*!< This holds the `QLabel` that shows the animation*/
	QScopedPointer<QGridLayout>	m_gridStatus;		/*!< This holds the Roll button */
	QScopedPointer<QGridLayout>	m_gridReset;		/*!< This holds the Reset button */
	QScopedPointer<QGridLayout>	m_gridWarning;		/*!< This holds the Quit button */
	QScopedPointer<Animation>	m_animationLabel;	/*!< The central `QLabel` that shows the animation */
	QScopedPointer<QLabel>		m_labelStatus;		/*!< The `QLabel` that prints the status of the app */
	QScopedPointer<QLabel>		m_labelWarning;		/*!< The `QLabel` that prints a warning if one cheats */
	QScopedPointer<QLabel>		m_labelNumberofRolls;	/*!< The `QLabel` that prints the number of total rolls */
	QScopedPointer<QAction>		m_actionRoll;		/*!< `QAction` for Roll */
	QScopedPointer<QAction>		m_actionReset;		/*!< `QAction` for Reset */
	QScopedPointer<QAction>		m_actionSettings;	/*!< `QAction` for Settings */
	QScopedPointer<QIcon>		m_exitIcon;		/*!< `QIcon` for exit */
	QScopedPointer<QIcon>		m_kdiceIcon;		/*!< `QIcon` as Logo */
	QScopedPointer<QSpinBox>	m_spinBox;		/*!< `QSpinBox` that directly selects the dice number. \sa KDice::reload(int num) */
	KSharedConfig::Ptr		m_sharedConfig;		/*!< `KConfig` object */
	QScopedPointer<KConfigGroup>	m_soundConfig;
	QScopedPointer<KD_InterSettings> m_internalSettings;	/*!< Object of type `InternalSettings` */
	bool				m_wantSound;		/*!< Boolean to be used for Settings configuration */
	bool				m_wantResetBtn;		/*!< Boolean to be used for Settings configuration */
	QScopedPointer<Dice> 		m_dice;			/*!< A ptr of type `Dice` */
	QScopedPointer<QShortcut>	m_quitShortcut;		/*!< A shortcut to quit (ESC) */

	void setupActions();		//!< Get actions ready.
	void setupConnections();	//!< Setup every  `connect()`
	void setupWidgets();		//!< Setup and get ready every widget of the app.
	void setupLayouts();		//!< Setup and get ready every layout and it's dimensions.

#ifdef ENABLE_SOUND
	QSoundEffect roll_sound;
#endif
};

#endif              // KDICE_H
