/*!< \author Πέτρος Σιλιγκούνας <petross404@gmail.com>
 * \file read_settings.cpp
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../kdice.h"

void KDice::loadSettings()
{
#ifdef ENABLE_SOUND
	m_wantSound = m_internalSettings->chkBoxSound();
#else
	/// It's silly to oversmart the program by editing the config, so hardcode this.
	m_internalSettings->setChkBoxSound( false );
#endif

#ifdef ENABLE_RESET
	m_wantResetBtn = m_internalSettings->chkBoxResetBtn();
#else
	/// It's silly to oversmart the program by editing the config, so hardcode this.
	m_internalSettings->setChkBoxResetBtn( false );
#endif

	m_internalSettings->save();
}

void KDice::saveSettings()
{
	m_internalSettings->setChkBoxResetBtn( m_wantResetBtn );
	m_internalSettings->setChkBoxSound( m_wantSound );
	m_internalSettings->save();
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
