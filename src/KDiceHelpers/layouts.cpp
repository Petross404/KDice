/*!< \author Πέτρος Σιλιγκούνας <petross404@gmail.com>
 * \file layouts.cpp
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../kdice.h"

void KDice::setupLayouts()
{
	m_gridLayout->addLayout( m_gridLabel.data(), 0, 0 );
	m_gridLayout->addLayout( m_gridStatus.data(), 1, 0 );
#ifdef ENABLE_RESET
	m_gridLayout->addLayout( m_gridReset.data(), 2, 0 );
#endif
	m_gridLayout->addLayout( m_gridWarning.data(), 3, 0 );

	m_gridLabel->addWidget( m_animationLabel.data(), 0, 0 );

	m_gridStatus->addWidget( m_labelStatus.data(), 0, 0 );
	m_gridStatus->addWidget( m_spinBox.data(), 0, 1 );
	m_gridStatus->addWidget( m_btnRoll.data(), 0, 2 );

	m_gridReset->addWidget( m_labelNumberofRolls.data(), 0, 0 );
	m_gridReset->setHorizontalSpacing( 180 );

	m_gridWarning->addWidget( m_labelWarning.data(), 0, 0 );
	m_gridWarning->addWidget( m_btnQuit.data(), 0, 1 );
	m_gridWarning->setHorizontalSpacing( 180 );
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
