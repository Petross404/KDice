/*!< \author Πέτρος Σιλιγκούνας <petross404@gmail.com>
 * \file connections.cpp
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../kdice.h"

void KDice::setupConnections()
{
	/// Connect spinBox to reload function and the label_warning
	connect( m_spinBox.data(), QOverload<int>::of( &QSpinBox::valueChanged ), this,
	         static_cast<void ( KDice::* )( int ) > ( &KDice::reload ) );

	connect( m_btnRoll.data(), &QPushButton::clicked, this,
	         static_cast<void ( KDice::* )( void )>( &KDice::reload ) );

	connect( m_btnRoll.data(), &QPushButton::clicked, this, &KDice::readyToRoll );

	connect( m_spinBox.data(), &QSpinBox::editingFinished, this, &KDice::printWarning );

	connect( m_animationLabel.data(), &Animation::startedAnimation, this, &KDice::disableWidgets );
	connect( m_animationLabel.data(), &Animation::finishedAnimation, this, &KDice::enableWidgets );

	/// Each and every time KDice is readyToRoll, re-read the configuration if it changed manually
	connect( this, &KDice::readyToRoll, m_internalSettings.data(), &KD_InterSettings::load );
	connect( this, &KDice::readyToRoll, this, &KDice::loadSettings );

	// 	connect(m_btnReset.data(), &QPushButton::clicked, this, &KDice::resetKDice);
	connect( m_btnQuit.data(), &QPushButton::clicked, this, &QApplication::quit );

	/// If the user hasn't rolled the dice yet (or the user hitted Reset already), then
	/// having the Reset button/Menu enabled is kind of pointless. Not that it wouldn't work
	/// Also, since isNumberOfRollsIncremented returns true and we want the widgets to be
	/// disabled, we should use ::setDisabled(true) since ::setEnabled would need false to be returned
	// 	connect(this, static_cast<bool (KDice::*)(bool)>(&KDice::isNumberOfRollsIncremented),
	// 	        m_btnReset.data(), QOverload<bool>::of(&QPushButton::setDisabled));
	connect( this, static_cast<bool ( KDice::* )( bool )>( &KDice::isNumberOfRollsIncremented ),
	         m_actionReset.data(), QOverload<bool>::of( &QAction::setDisabled ) );

	connect( m_quitShortcut.data(), &QShortcut::activated, this, &QApplication::quit );
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
