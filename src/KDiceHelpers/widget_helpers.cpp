/*!< \author Πέτρος Σιλιγκούνας <petross404@gmail.com>
 * \file widget_helpers.cpp
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../kdice.h"

void KDice::disableWidgets()
{
	/// Now that movie is started, set the following...
	m_btnRoll->setFocus();
	m_actionRoll->setEnabled( false );
	m_btnRoll->setEnabled( false );
	/// We don't want to hit Reset when animation is running
	m_actionReset->setEnabled( false );

	/// ...also set the QLabel that show the status
	m_labelStatus->setText( i18n( "Rolling..." ) );
	m_spinBox->setEnabled( false );
}

void KDice::enableWidgets()
{
	m_animationLabel->setPixmap( *m_image.data() );
	m_animationLabel->show();
	m_btnRoll->setEnabled( true );
	m_btnRoll->setFocus();
	m_actionReset->setEnabled( true );
	m_actionRoll->setEnabled( true );
}

/// This function setups the widgets and their attibutes
void KDice::setupWidgets()
{
	m_btnRoll->setIcon( QIcon::fromTheme( "roll", *m_kdiceIcon.data() ) );
	m_btnRoll->setToolTip( i18n( "Roll the dice" ) );
	m_btnQuit->setIcon( QIcon::fromTheme( "application-exit", *m_exitIcon.data() ) );
	m_btnQuit->setToolTip( i18n( "Quit application" ) );

	m_actionReset->setEnabled( false );

	m_labelStatus->clear();
	m_labelWarning->clear();
	m_labelNumberofRolls->clear();
	m_labelStatus->setText( i18n( "Haven't rolled yet" ) );

	m_spinBox->cleanText();
	m_spinBox->clear();
	m_spinBox->setRange( 1, 6 );
	m_spinBox->setToolTip( i18n( "Use this to cheat :p" ) );

	m_animationLabel->show();
}

// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
