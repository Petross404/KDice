/*!< \author Πέτρος Σιλιγκούνας <petross404@gmail.com>
 * \file actions.cpp
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../kdice.h"

void KDice::setupActions()
{
	m_actionRoll->setText( i18n( "&Roll the Dice" ) );
	m_actionRoll->setIcon( QIcon::fromTheme( "roll" ) );

	m_actionReset->setText( i18n( "&Reset KDice" ) );
	m_actionReset->setIcon( QIcon::fromTheme( "reload" ) );

	actionCollection()->setDefaultShortcut( m_actionRoll.data(), Qt::Key_R );
	actionCollection()->addAction( "Roll", m_actionRoll.data() );
	actionCollection()->setDefaultShortcut( m_actionReset.data(), Qt::Key_E );
	actionCollection()->addAction( "Reset", m_actionReset.data() );

	connect( m_actionRoll.data(), &QAction::triggered, this,
	         static_cast<void ( KDice::* )( void )> ( &KDice::reload ) );
	connect( m_actionReset.data(), &QAction::triggered, this, &KDice::resetKDice );

	m_actionSettings->setText( i18n( "&Settings" ) );
	m_actionSettings->setIcon( QIcon::fromTheme( "settings-configure" ) );
	actionCollection()->addAction( "Settings", m_actionSettings.data() );
	connect( m_actionSettings.data(), &QAction::triggered, this, &KDice::showSettings );

	KStandardAction::quit( qApp, &QApplication::quit, actionCollection() );
	KStandardAction::preferences( this, &KDice::showSettings, actionCollection() );

	setupGUI( Default, ":/resources/KDiceui.rc" );
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
