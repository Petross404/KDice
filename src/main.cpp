/*!
 * \author Petros S <petross404@gmail.com>
 * \file main.cpp
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "kdice.h"
#include <QtWidgets/QApplication>
#include <QtCore/QCommandLineParser>
#include <QtCore/QCommandLineOption>
#include <QtCore/QStringLiteral>
#include <KAboutData>
#include <KLocalizedString>
#include <KMessageBox>
#include <QString>

int main( int argc, char* argv[] )
{
	QApplication* app = new QApplication( argc, argv );
	QApplication::setWindowIcon( QIcon( ":/resources/images/dice.ico" ) );

	KLocalizedString::setApplicationDomain( "KDice" );
	KAboutData aboutData(
	        QStringLiteral( "KDice" ),
	        i18n( "KDice" ),
	        i18n( QString( "%1" ).arg( KDice_VERSION ).toLatin1() ),
	        i18n( "Simple Qt5/KF5 game to roll the dice" ),
	        KAboutLicense::GPL,
	        QStringLiteral( "https://github.com/Petross404/KDice" ),
	        i18n( "Git commit hash : " GIT_COMMIT_HASH ),
	        QStringLiteral( "submit@bugs.kde.org" ) );

	aboutData.addAuthor( i18n( "Petros Siligkounas" ), i18n( "Author" ),
	                     QStringLiteral( "mailto:petross404@gmail.com" ) );

	aboutData.addCredit( i18n( "Aidonitsa" ),
	                     i18n( "My girlfriend who tolerates my hobby with C++."
	                           " Without her patience, none of these could be possible." ) );

	KAboutData::setApplicationData( aboutData );

	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();
	aboutData.setupCommandLine( &parser );
	QCommandLineOption diceNumber( QStringList() << "n" << "number",
	                               QApplication::translate( "Main",
	                                               "number that dice has to start with" ), "number", "0" );

	parser.addOption( diceNumber );
	parser.process( *app );
	int number = QString( "%1" ).arg( parser.value( diceNumber ) ).toInt();

	//Fire up the GUI
	QScopedPointer<KDice> mainWindow( new KDice );
	mainWindow->show();

	return app->exec();
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
