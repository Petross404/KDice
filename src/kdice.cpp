/*!<
 * \author Petros S <petross404@gmail.com>
 * \file kdice.cpp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <kdicesettings.h>
#include <KConfigDialogManager>
#include <QtCore/QStandardPaths>
#include <QtCore/QSystemSemaphore>
#include <QtCore/QSharedMemory>
#include "kdice.h"
#include "config/SettingsWidgets/soundWidget.h"

Q_LOGGING_CATEGORY( LOG_KDICE, "KDice" )

KDice::KDice( QWidget* parent )
	: KXmlGuiWindow( parent )
	, m_numberOfRolls( 0 )
	, m_image( new QPixmap( ":/resources/images/128-apps-dice.png" ) )
	, m_btnRoll( new QPushButton( i18n( "&Roll the dice" ), this ) )
	, m_btnQuit( new QPushButton( i18n( "&Quit" ), this ) )
	, m_widgetCentral( new QWidget )
	, m_gridLayout( new QGridLayout )
	, m_gridLabel( new QGridLayout )
	, m_gridStatus( new QGridLayout )
	, m_gridReset( new QGridLayout )
	, m_gridWarning( new QGridLayout )
	, m_animationLabel( new Animation( m_widgetCentral.data() ) )
	, m_labelStatus( new QLabel( this ) )
	, m_labelWarning( new QLabel( this ) )
	, m_labelNumberofRolls( new QLabel( this ) )
	, m_actionRoll( new QAction( this ) )
	, m_actionReset( new QAction( this ) )
	, m_actionSettings( new QAction( this ) )
	, m_exitIcon( new QIcon( ":/resources/images/exit.ico" ) )
	, m_kdiceIcon( new QIcon( ":/resources/images/dice.ico" ) )
	, m_spinBox( new QSpinBox( this ) )
	, m_sharedConfig( KSharedConfig::openConfig( "kdicerc", KSharedConfig::OpenFlag::SimpleConfig,
	                  QStandardPaths::AppConfigLocation ) )
	, m_soundConfig( new KConfigGroup( m_sharedConfig.data(), "Sound" ) )
	, m_internalSettings( KD_InterSettings::self() )
	, m_dice( Dice::instance() )
	, m_quitShortcut( new QShortcut( tr( "Esc", "File|Quit" ), this ) )
{
	m_internalSettings->load();
	loadSettings();
	setupLayouts();
	setupWidgets();
	setupActions();
	setupConnections();

	/// Hardcode anything related about the geometry and the layouts.
	setCentralWidget( m_widgetCentral.data() );
	centralWidget()->setLayout( m_gridLayout.data() );
	setMinimumSize( 535, 600 );
	setMaximumSize( 535, 600 );
}

KDice::~KDice() {}

/// Before showing the image, it plays a small animation of a rolling dice
/// for enhanced user experience.
void KDice::animateDice()
{
	/// To re-evaluate some conditions at every roll, we must known
	/// when it's about to happen.
	//emit readyToRoll();

#ifdef ENABLE_SOUND

	if ( m_wantSound )
	{
		roll_sound.play();
	}

#endif  /// ENABLE_SOUND

	m_animationLabel->replay();

	/// Increment how many times the user reloaded and emit accordingly so
	/// that the reset btn is enabled only when at least one roll happened
	++m_numberOfRolls;

	if ( m_numberOfRolls ) { emit isNumberOfRollsIncremented( true ); }
}

/// This is the random reload function. After the animation is over, a dice object
/// is rolled. Look at Dice/dice.cpp for more details.
void KDice::reload()
{
	animateDice();
	m_dice->roll();
	m_animationLabel->update( m_dice->getNumber() );
}

/// This is a reload function. After the animation is over,
/// a dice is given a "hardcoded" value. Look at Dice/dice.cpp for more details.
/// Actually this function doesn't even need one Dice object.
/// It uses a known int value, it doesn't *have* to use Dice::get_number to get one randomly.
void KDice::reload( int number )
{
	animateDice();
	m_dice->setNumber( number );
	m_animationLabel->update( m_dice->getNumber() );
}

void KDice::resetKDice()
{
	m_numberOfRolls = 0;
	setupWidgets();
	m_animationLabel->setPixmap( QPixmap( ":/resources/images/128-apps-dice.png" ) );
}

void KDice::printNumberOfRolls()
{
	QString strNumberOfRolls = "You have rolled " + QString( "%1" ).arg( m_numberOfRolls ) +
	                           ( m_numberOfRolls == 1 ? " time" : " times" );
	m_labelNumberofRolls->setText( i18n( strNumberOfRolls.toLatin1() ) );
}

void KDice::printWarning()
{
	m_labelWarning->setText( i18n( "Did you cheat?" ) );
}

void KDice::showSettings()
{
	if ( KConfigDialog::showDialog( QStringLiteral( "Settings" ) ) )
	{
		return;
	}

	// m_internalSettings is a class KDice member and initialized in it's ctor.
	// m_internalSettings = InternalSettingsPtr(new KDiceSettings::InternalSettings());
	m_internalSettings->load();
	KConfigDialog* dialog = new KConfigDialog( this, "Settings", m_internalSettings.data() );

	dialog->setFaceType( KPageDialog::List );
	soundWidget*	w_sound = new soundWidget( this );
	ResetBtnWidget*	w_reset = new ResetBtnWidget( this );
#ifndef ENABLE_SOUND
	w_sound->setEnabled( false );
#endif

	dialog->addPage( w_reset, i18n( "General" ) );
	dialog->addPage( w_sound, i18n( "Sound" ) );

	connect( dialog, &KConfigDialog::settingsChanged, m_internalSettings.data(), &KD_InterSettings::load );
	connect( m_internalSettings.data(), &KD_InterSettings::configChanged, m_internalSettings.data(),
	         &KD_InterSettings::load );
	connect( m_internalSettings.data(), &KD_InterSettings::configChanged, this, &KDice::loadSettings );

	m_wantSound = m_internalSettings->chkBoxSound();
	m_wantResetBtn = m_internalSettings->chkBoxResetBtn();

	dialog->setAttribute( Qt::WA_DeleteOnClose );
	dialog->show();
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
