[![pipeline status](https://gitlab.com/Petross404/KDice/badges/no-ui_/pipeline.svg)](https://gitlab.com/Petross404/KDice/commits/no-ui_)

You need Qt ( >=5.4 ), KF5 ( >= 5.40) and CMake 3 or higher

KDice is a free game to roll the dice with Qt/KF5. The menus aren't shown since I use Active Window Control applet on Latte Dock.
![Alt text](https://i.imgur.com/hleX2m7.png)

-- Build instructions --
```
 git clone https://gitlab.com/Petross404/KDice.git
 cd KDice && mkdir build && cd build
 cmake .. && make
 ./KDice
```

This is a project only for fun and learning purposes. I wanted to mess with Qt5, C++ and cmake's 
conditional compilation. 

This project doesn't pretend to be written by an expert but you are free to read, borrow or modify the code for 
whatever reason you like.